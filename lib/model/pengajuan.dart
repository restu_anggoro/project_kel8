class Pengajuan {
  String? kodePengajuan;
  String? tglPengajuan;
  String? npmPeminjam;
  String? namaPeminjam;
  String? mhsProdi;
  String? noHandphone;

  Pengajuan({
    this.kodePengajuan,
    this.tglPengajuan,
    this.npmPeminjam,
    this.namaPeminjam,
    this.mhsProdi,
    this.noHandphone,
  });

  factory Pengajuan.fromJson(Map<String, dynamic> json) => Pengajuan(
        kodePengajuan: json['kodePengajuan'],
        tglPengajuan: json['tglPengajuan'],
        npmPeminjam: json['npmPeminjam'],
        namaPeminjam: json['namaPeminjam'],
        mhsProdi: json['mhsProdi'],
        noHandphone: json['noHandphone'],
      );

  Map<String, dynamic> toJson() => {
        'kodePengajuan': this.kodePengajuan,
        'tglPengajuan': this.tglPengajuan,
        'npmPeminjam': this.npmPeminjam,
        'namaPeminjam': this.namaPeminjam,
        'mhsProdi': this.mhsProdi,
        'noHandphone': this.noHandphone,
      };
}
