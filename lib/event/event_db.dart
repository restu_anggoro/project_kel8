import 'dart:convert';
import 'package:get/get.dart';
import 'package:project_kel_8/config/api.dart';
import 'package:project_kel_8/event/event_pref.dart';
import 'package:project_kel_8/model/barang.dart';
import 'package:project_kel_8/model/mahasiswa.dart';
import 'package:project_kel_8/model/pengajuan.dart';
import 'package:project_kel_8/model/user.dart';
import 'package:http/http.dart' as http;
import 'package:project_kel_8/screen/admin/add_update_mahasiswa.dart';
import 'package:project_kel_8/screen/login.dart';
import 'package:project_kel_8/widget/info.dart';

class EventDb {
  static Future<User?> login(String username, String pass) async {
    User? user;

    try {
      var response = await http.post(Uri.parse(Api.login), body: {
        'username': username,
        'pass': pass,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);

        if (responBody['success']) {
          user = User.fromJson(responBody['user']);
          EventPref.saveUser(user);
          Info.snackbar('Login Berhasil');
          Future.delayed(Duration(milliseconds: 1700), () {
            Get.off(
              Login(),
            );
          });
        } else {
          Info.snackbar('Login Gagal');
        }
      } else {
        Info.snackbar('Request Login Gagal');
      }
    } catch (e) {
      print(e);
    }
    return user;
  }

  static Future<List<User>> getUser() async {
    List<User> listUser = [];

    try {
      var response = await http.get(Uri.parse(Api.getUsers));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var users = responBody['user'];

          users.forEach((user) {
            listUser.add(User.fromJson(user));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listUser;
  }

  static Future<String> addUser(
      String name, String username, String pass, String role) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addUser), body: {
        'name': name,
        'username': username,
        'pass': pass,
        'role': role
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add User Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> UpdateUser(
    String id,
    String name,
    String username,
    String pass,
    String role,
  ) async {
    try {
      var response = await http.post(Uri.parse(Api.updateUser), body: {
        'id': id,
        'name': name,
        'username': username,
        'pass': pass,
        'role': role
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update User');
        } else {
          Info.snackbar('Gagal Update User');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deleteUser(String id) async {
    try {
      var response =
          await http.post(Uri.parse(Api.deleteUser), body: {'id': id});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete User');
        } else {
          Info.snackbar('Gagal Delete User');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<List<Mahasiswa>> getMahasiswa() async {
    List<Mahasiswa> listMahasiswa = [];

    try {
      var response = await http.get(Uri.parse(Api.getMahasiswa));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var mahasiswa = responBody['mahasiswa'];

          mahasiswa.forEach((mahasiswa) {
            listMahasiswa.add(Mahasiswa.fromJson(mahasiswa));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listMahasiswa;
  }

  static Future<String> AddMahasiswa(String mhsNpm, String mhsNama,
      String mhsAlamat, String mhsFakultas, String MhsProdi) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addMahasiswa), body: {
        'mhsNpm': mhsNpm,
        'mhsNama': mhsNama,
        'mhsAlamat': mhsAlamat,
        'mhsFakultas': mhsFakultas,
        'MhsProdi': MhsProdi,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Mahasiswa Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  static Future<void> UpdateMahasiswa(String mhsNpm, String mhsNama,
      String mhsAlamat, String mhsFakultas, String MhsProdi) async {
    try {
      var response = await http.post(Uri.parse(Api.updateMahasiswa), body: {
        'mhsNpm': mhsNpm,
        'mhsNama': mhsNama,
        'mhsAlamat': mhsAlamat,
        'mhsFakultas': mhsFakultas,
        'MhsProdi': MhsProdi
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Mahasiswa');
        } else {
          Info.snackbar('Gagal Update Mahasiswa');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  static Future<void> deleteMahasiswa(String mhsNpm) async {
    try {
      var response = await http
          .post(Uri.parse(Api.deleteMahasiswa), body: {'mhsNpm': mhsNpm});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Mahasiswa');
        } else {
          Info.snackbar('Gagal Delete Mahasiswa');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  //Barang
  static Future<List<Barang>> getBarang() async {
    List<Barang> listBarang = [];

    try {
      var response = await http.get(Uri.parse(Api.getBarang));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var barang = responBody['barang'];

          barang.forEach((barang) {
            listBarang.add(Barang.fromJson(barang));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listBarang;
  }
  //AddBarang

  static Future<String> AddBarang(
      String kodeBarang, String namaBarang, String jumlahBarang) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addBarang), body: {
        'kodeBarang': kodeBarang,
        'namaBarang': namaBarang,
        'jumlahBarang': jumlahBarang,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Barang Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  //Update Barang
  static Future<void> UpdateBarang(
      String kodeBarang, String namaBarang, String jumlahBarang) async {
    try {
      var response = await http.post(Uri.parse(Api.updateBarang), body: {
        'kodeBarang': kodeBarang,
        'namaBarang': namaBarang,
        'jumlahBarang': jumlahBarang,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Barang');
        } else {
          Info.snackbar('Gagal Update Barang');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  //delete barang
  static Future<void> deleteBarang(String kodeBarang) async {
    try {
      var response = await http
          .post(Uri.parse(Api.deleteBarang), body: {'kodeBarang': kodeBarang});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Mahasiswa');
        } else {
          Info.snackbar('Gagal Delete Mahasiswa');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  //Pengajuan
  static Future<List<Pengajuan>> getPengajuan() async {
    List<Pengajuan> listPengajuan = [];

    try {
      var response = await http.get(Uri.parse(Api.getPengajuan));

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          var pengajuan = responBody['pengajuan'];

          pengajuan.forEach((pengajuan) {
            listPengajuan.add(Pengajuan.fromJson(pengajuan));
          });
        }
      }
    } catch (e) {
      print(e);
    }

    return listPengajuan;
  }
  //AddPengajuan

  static Future<String> AddPengajuan(
      String kodePengajuan,
      String tglPengajuan,
      String npmPeminjam,
      String namaPeminjam,
      String mhsProdi,
      String noHandphone) async {
    String reason;

    try {
      var response = await http.post(Uri.parse(Api.addPengajuan), body: {
        'kodePengajuan': kodePengajuan,
        'tglPengajuan': tglPengajuan,
        'npmPeminjam': npmPeminjam,
        'namaPeminjam': namaPeminjam,
        'mhsProdi': mhsProdi,
        'noHandphone': noHandphone,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          reason = 'Add Pengajuan Berhasil';
        } else {
          reason = responBody['reason'];
        }
      } else {
        reason = "Request Gagal";
      }
    } catch (e) {
      print(e);
      reason = e.toString();
    }

    return reason;
  }

  //Update Pengajuan
  static Future<void> UpdatePengajuan(
      String kodePengajuan,
      String tglPengajuan,
      String npmPeminjam,
      String namaPeminjam,
      String mhsProdi,
      String noHandphone) async {
    try {
      var response = await http.post(Uri.parse(Api.updatePengajuan), body: {
        'kodePengajuan': kodePengajuan,
        'tglPengajuan': tglPengajuan,
        'npmPeminjam': npmPeminjam,
        'namaPeminjam': namaPeminjam,
        'mhsProdi': mhsProdi,
        'noHandphone': noHandphone,
      });

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Update Pengajuan');
        } else {
          Info.snackbar('Gagal Update Pengajuan');
        }
      }
    } catch (e) {
      print(e);
    }
  }

  //delete pengajuan
  static Future<void> deletePengajuan(String kodePengajuan) async {
    try {
      var response = await http.post(Uri.parse(Api.deletePengajuan),
          body: {'kodePengajuan': kodePengajuan});

      if (response.statusCode == 200) {
        var responBody = jsonDecode(response.body);
        if (responBody['success']) {
          Info.snackbar('Berhasil Delete Pengajuan');
        } else {
          Info.snackbar('Gagal Delete Pengajuan');
        }
      }
    } catch (e) {
      print(e);
    }
  }
}
