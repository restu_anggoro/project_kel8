import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:get/get.dart';
import 'package:project_kel_8/config/asset.dart';
import 'package:project_kel_8/event/event_db.dart';
import 'package:project_kel_8/screen/admin/list_pengajuan.dart';
import 'package:project_kel_8/widget/info.dart';

import '../../model/pengajuan.dart';

class AddUpdatePengajuan extends StatefulWidget {
  final Pengajuan? pengajuan;
  AddUpdatePengajuan({this.pengajuan});

  @override
  State<AddUpdatePengajuan> createState() => _AddUpdatePengajuanState();
}

class _AddUpdatePengajuanState extends State<AddUpdatePengajuan> {
  var _formKey = GlobalKey<FormState>();
  var _controllerkode = TextEditingController();
  var _controllertgl = TextEditingController();
  var _controllernpm = TextEditingController();
  var _controllernama = TextEditingController();
  var _controllerprodi = TextEditingController();
  var _controllerno = TextEditingController();

  bool _isHidden = true;
  @override
  void initState() {
    // TODO: implement initState
    if (widget.pengajuan != null) {
      _controllerkode.text = widget.pengajuan!.kodePengajuan!;
      _controllertgl.text = widget.pengajuan!.tglPengajuan!;
      _controllernpm.text = widget.pengajuan!.npmPeminjam!;
      _controllernama.text = widget.pengajuan!.namaPeminjam!;
      _controllerprodi.text = widget.pengajuan!.mhsProdi!;
      _controllerno.text = widget.pengajuan!.noHandphone!;
    }
    super.initState();
  }

  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      appBar: AppBar(
        // titleSpacing: 0,
        title: widget.pengajuan != null
            ? Text('Update Pengajuan')
            : Text('Tambah Pengajuan'),
        backgroundColor: Asset.colorPrimary,
      ),
      body: Stack(
        children: [
          Form(
            key: _formKey,
            child: ListView(
              padding: EdgeInsets.all(16),
              children: [
                TextFormField(
                  enabled: widget.pengajuan == null ? true : false,
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerkode,
                  decoration: InputDecoration(
                      labelText: "Kode",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllertgl,
                  decoration: InputDecoration(
                      labelText: "Tanggal",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllernpm,
                  decoration: InputDecoration(
                      labelText: "NPM",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllernama,
                  decoration: InputDecoration(
                      labelText: "Nama",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerprodi,
                  decoration: InputDecoration(
                      labelText: "Prodi",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                TextFormField(
                  validator: (value) => value == '' ? 'Jangan Kosong' : null,
                  controller: _controllerno,
                  decoration: InputDecoration(
                      labelText: "No",
                      border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(10))),
                ),
                SizedBox(
                  height: 10,
                ),
                ElevatedButton(
                  onPressed: () async {
                    if (_formKey.currentState!.validate()) {
                      if (widget.pengajuan == null) {
                        String message = await EventDb.AddPengajuan(
                          _controllerkode.text,
                          _controllertgl.text,
                          _controllernpm.text,
                          _controllernama.text,
                          _controllerprodi.text,
                          _controllerno.text,
                        );
                        Info.snackbar(message);
                        if (message.contains('Berhasil')) {
                          _controllerkode.clear();
                          _controllertgl.clear();
                          _controllernpm.clear();
                          _controllernama.clear();
                          _controllerprodi.clear();
                          _controllerno.clear();
                          Get.off(
                            ListPengajuan(),
                          );
                        }
                      } else {
                        EventDb.UpdatePengajuan(
                          _controllerkode.text,
                          _controllertgl.text,
                          _controllernpm.text,
                          _controllernama.text,
                          _controllerprodi.text,
                          _controllerno.text,
                        );
                      }
                    }
                  },
                  child: Text(
                    widget.pengajuan == null ? 'Simpan' : 'Ubah',
                    style: TextStyle(fontSize: 16),
                  ),
                  style: ElevatedButton.styleFrom(
                      primary: Asset.colorAccent,
                      fixedSize: Size.fromHeight(50),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5))),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
