import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';
import 'package:flutter/src/widgets/placeholder.dart';
import 'package:flutter/widgets.dart';
import 'package:project_kel_8/config/asset.dart';
import 'package:project_kel_8/event/event_db.dart';

class Login extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Menu Login',
      theme: ThemeData(),
      home: LoginPage(),
    );
  }
}

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login Ke Akun Anda'),
        backgroundColor: Color.fromARGB(255, 109, 11, 11),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(16.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Image.network(
                  'https://tse1.mm.bing.net/th?id=OIP.34XwLxi7zTEph4zmJM7prAHaEK&pid=Api&P=0&w=300&h=300'),
              SizedBox(height: 24.0),
              Text(
                'Universitas Teknokrat Indonesia',
                style: TextStyle(
                  fontSize: 24.0,
                  color: Color.fromARGB(255, 109, 11, 11),
                  fontWeight: FontWeight.bold,
                ),
              ),
              SizedBox(height: 24.0),
              TextField(
                controller: usernameController,
                decoration: InputDecoration(
                  hintText: 'Masukkan Username',
                  labelText: 'Username',
                ),
              ),
              SizedBox(height: 12.0),
              TextField(
                controller: passwordController,
                obscureText: true,
                decoration: InputDecoration(
                  hintText: 'Masukkan Password',
                  labelText: 'Password',
                ),
              ),
              SizedBox(height: 24.0),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  ElevatedButton(
                    onPressed: () {
                      // aksi ketika tombol kiri ditekan
                    },
                    style: ElevatedButton.styleFrom(
                      primary: Color.fromARGB(
                          255, 109, 11, 11), // warna latar belakang tombol
                      onPrimary: Colors.white, // warna teks tombol
                    ),
                    child: Text('Login'),
                  ),
                  ElevatedButton(
                    onPressed: () {
                      // aksi ketika tombol kanan ditekan
                    },
                    style: ElevatedButton.styleFrom(
                      primary: Color.fromARGB(
                          255, 109, 11, 11), // warna latar belakang tombol
                      onPrimary: Colors.white, // warna teks tombol
                    ),
                    child: Text('Daftar'),
                  ),
                ],
              ),
              SizedBox(height: 20),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Belum Punya Akun?',
                    style: TextStyle(
                      fontSize: 15.0,
                      color: Color.fromARGB(255, 0, 0, 0),
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                  Text(
                    ' Lupa Password',
                    style: TextStyle(
                      fontSize: 15.0,
                      color: Colors.blue,
                      fontWeight: FontWeight.normal,
                    ),
                  ),
                ],
              ),
              SizedBox(height: 10),
              Text(
                '----- or Login with -----',
                style: TextStyle(
                  fontSize: 15.0,
                  color: Color.fromARGB(255, 0, 0, 0),
                  fontWeight: FontWeight.normal,
                ),
              ),
              SizedBox(height: 10),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                    height: 40,
                    width: 40,
                    margin: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Color.fromARGB(255, 211, 211, 211)),
                    child: CircleAvatar(
                      radius: 50,
                      backgroundColor: Color.fromARGB(255, 202, 202, 202),
                      backgroundImage: NetworkImage(
                          "https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Google_%22G%22_Logo.svg/2008px-Google_%22G%22_Logo.svg.png"),
                    ),
                  ),
                  Container(
                    height: 40,
                    width: 40,
                    margin: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        color: Color.fromARGB(255, 211, 211, 211)),
                    child: CircleAvatar(
                      radius: 50,
                      backgroundColor: Color.fromARGB(255, 202, 202, 202),
                      backgroundImage: NetworkImage(
                          "https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Facebook_f_logo_%282019%29.svg/2048px-Facebook_f_logo_%282019%29.svg.png"),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
